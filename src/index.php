<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>時刻表示</title>
</head>
<body>
<?php
//参考:https://syncer.jp/php-date-strtotime
// UNIX TIMESTAMPを取得
$timestamp = time() ;
// date()で日時を出力
$date = date( "Y/n/d/G:i:s", $timestamp ) ;
echo $date ;
?>

</body>
</html>